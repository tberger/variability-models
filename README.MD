This repository was recently deleted by bitbucket, since it was still relying on mercurial. I've recreated it, but lost the readme.

Main paper:

Thorsten Berger, Steven She, Rafael Lotufo, Andrzej Wasowski, Krzysztof Czarnecki, "A Study of Variability Models and Languages in the Systems Software Domain," IEEE Transactions on Software Engineering, vol. 39, no. 12, pp. 1611–1640, 2013.
